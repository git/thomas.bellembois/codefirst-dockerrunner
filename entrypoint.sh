#!/usr/bin/env bash

HostName=""
DockerNetworkName=""
DockerPathPrefix=""
Scheme=""
MaxAllowedContainers=""
Test=""

if [ ! -z "$TEST" ]
then
    Test="-test"
fi

if [ ! -z "$HOSTNAME" ]
then
    HostName="-hostname $HOSTNAME"
fi

if [ ! -z "$SCHEME" ]
then
    Scheme="-scheme $SCHEME"
fi

if [ ! -z "$DOCKERNETWORKNAME" ]
then
    DockerNetworkName="-dockernetworkname $DOCKERNETWORKNAME"
fi

if [ ! -z "$DOCKERPATHPREFIX" ]
then
    DockerPathPrefix="-dockerpathprefix $DOCKERPATHPREFIX"
fi

if [ ! -z "$MAXALLOWEDCONTAINERS" ]
then
    MaxAllowedContainers="-maxallowedcontainers $MAXALLOWEDCONTAINERS"
fi

echo $HostName
echo $DockerNetworkName
echo $DockerPathPrefix
echo $MaxAllowedContainers
echo $Test

/go/bin/codefirst-dockerrunner $Scheme $HostName $DockerNetworkName $DockerPathPrefix $MaxAllowedContainers $Test