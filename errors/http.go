package errors

import (
	"fmt"
	"net/http"
)

func InternalServerError(w http.ResponseWriter, r *http.Request, err error) {
	fmt.Println(err)
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(err.Error()))
}

func BadRequestError(w http.ResponseWriter, r *http.Request, err error) {
	fmt.Println(err)
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(err.Error()))
}
