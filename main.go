package main

import (
	"embed"
	"flag"
	"net/http"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner/v2/globals"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner/v2/handlers"
	"github.com/gorilla/mux"
)

var (
	//go:embed wasm/*
	embedWasmBox embed.FS
	//go:embed static/*
	embedStaticBox embed.FS
)

func init() {
	flag.StringVar(&globals.Scheme, "scheme", "http", "application scheme")
	flag.StringVar(&globals.HostName, "hostname", "localhost:8081", "full hostname (ie. codefirst.uca.fr)")
	flag.StringVar(&globals.DockerNetworkName, "dockernetworkname", "cicd_net", "docker network name for running containers")
	flag.StringVar(&globals.DockerPathPrefix, "dockerpathprefix", "containers", "common path prefix for running containers")
	flag.IntVar(&globals.MaxAllowedContainers, "maxallowedcontainers", 1, "maximum allowed running containers per user")
	flag.BoolVar(&globals.Test, "test", false, "developper flag")

	flag.Parse()
}

func main() {
	router := mux.NewRouter()

	http.Handle("/wasm/", http.FileServer(http.FS(embedWasmBox)))
	http.Handle("/static/", http.FileServer(http.FS(embedStaticBox)))
	http.HandleFunc("/ws/", handlers.WebSockerHandler)
	http.Handle("/", router)

	if err := http.ListenAndServe(":8081", nil); err != nil {
		panic("error running the server:" + err.Error())
	}
}
