package api

import (
	"context"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/callbacks"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/models"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner/v2/globals"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

func GetConfig(wsCtx context.Context, wsConnection *websocket.Conn) {
	wsjson.Write(wsCtx, wsConnection, messages.WSMessage{
		Action: callbacks.AfterGetConfigCallback,
		Config: models.CodefirstConfig{
			MaxAllowedContainers: globals.MaxAllowedContainers,
		},
	},
	)
}
