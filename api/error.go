package api

import (
	"context"
	"fmt"

	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/errors"
	"codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-dockerrunner-common/v2/messages"
	"nhooyr.io/websocket"
	"nhooyr.io/websocket/wsjson"
)

func sendError(ctx context.Context, wsConnection *websocket.Conn, err errors.AppError) {
	fmt.Println(err)
	wsjson.Write(ctx, wsConnection, messages.WSMessage{Error: &err})
}
