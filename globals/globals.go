package globals

import (
	"github.com/gorilla/schema"

	"github.com/docker/docker/client"
)

var (
	HostName             string
	Scheme               string
	DockerNetworkName    string
	DockerPathPrefix     string
	MaxAllowedContainers int
	Test                 bool

	DockerClient  *client.Client
	SchemaDecoder *schema.Decoder
)

func init() {
	var err error
	if DockerClient, err = client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation()); err != nil {
		panic(err)
	}

	SchemaDecoder = schema.NewDecoder()
}
